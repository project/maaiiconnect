(function (Drupal, $) {

  "use strict";

  Drupal.behaviors.admin = {
    attach: function (context, settings) {
      const SERVICE_ACCOUNT_REGEX = /^[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9](\.[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9])*\.(maaiiconnect|m800)\.(com|cn)$/;
      const $serviceAccountInput = $('#edit-service-account');
      const $launchButton = $('#edit-launch-dashboard');

      const serviceAccount = $serviceAccountInput.val() || '';

      if (serviceAccount.match(SERVICE_ACCOUNT_REGEX)) {
        $launchButton.removeAttr('disabled');
        $launchButton.removeClass('is-disabled');

        $launchButton.click(() => {
          window.open(`https://${$launchButton.attr('data-service-account')}`, '_blank');
        });
      }
    }
  };

})(Drupal, jQuery);
