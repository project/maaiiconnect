(function loadMcwc() {
  try {
    const { isAdminPage, config: { serviceAccount, mcwcSrc } } = drupalSettings.maaiiconnect;

    if (!isAdminPage) {
      window.mcwcSettings = {
        serviceName: serviceAccount,
      };

      !function(e,t,c){var n,s=e.getElementsByTagName(t)[e.getElementsByTagName(t).length-1];e.getElementById(c)||(n=e.createElement(t),n.id=c,n.defer=!0,n.src=mcwcSrc,s.parentNode.insertBefore(n,s))}(document,'script','mcwc-sdk')
    }
  } catch (mcwcLoadingError) {
    // Just don't crash the JS thread, no actual handling is available
    console.error(mcwcLoadingError);
  }
}());
